﻿using UnityEngine;
using System.Collections;

public class ColorChanger : MonoBehaviour
{
	[SerializeField]
	private Color[] _colors;
	[SerializeField]
	private Sprite[] _sprite;
	[SerializeField]
	private SpriteRenderer _playerSpriteRenderer;

	private readonly string _colorString = "colorId";
	
	private PathController _pathController;
	private SpriteRenderer _prefabSpriteRenderer;

	private int _colorId;

	void Start()
	{
		if (_colors == null || _sprite==null || _sprite.Length!=_colors.Length)
		{
			Debug.LogError("Colors count must be equels sprites count (and not null)");
		}

		GameObject _blockPrefab;

		_blockPrefab = Resources.Load<GameObject>("Prefabs/Block");
		_pathController = FindObjectOfType<PathController>();

		if(_pathController==null)
			Debug.LogError("PathController is null");

		if (_blockPrefab == null)
			Debug.LogError("Can not load Prefabs/Block");
		else
		{
			_prefabSpriteRenderer = _blockPrefab.GetComponent<SpriteRenderer>();

			if (_prefabSpriteRenderer == null)
			{
				Debug.LogError("BlockPrefab dont have SpriteRenderer");
			}
			else
			{
				_colorId = PlayerPrefs.GetInt(_colorString,0);

				UpdateColor();
			}
		}

	}

	public void ChangeColor()
	{
		NextColorId();

		UpdateColor();

		PlayerPrefs.SetInt(_colorString, _colorId);
	}

	private void UpdateColor()
	{
		_prefabSpriteRenderer.color = _colors[_colorId];
		_pathController.UpdatePathCollor(_colors[_colorId]);

		_playerSpriteRenderer.sprite = _sprite[_colorId];
	}

	private void NextColorId()
	{
		_colorId++;

		if (_colorId >= _colors.Length)
		{
			_colorId = 0;
		}
	}
}
