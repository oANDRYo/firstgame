using UnityEngine;
using UnityEngine.UI;

public class FPS : MonoBehaviour {
    [SerializeField] private Text _uiTextFps;
    //
    private float _accum; // FPS accumulated over the interval
    private int _frames; // Frames drawn over the interval
    private float _timeleft; // Left time for current interval

    private void Awake() {
        //Destroy(gameObject);
    }

    private void Update() {
        _timeleft -= Time.deltaTime;
        _accum += Time.timeScale / Time.deltaTime;
        ++_frames;

        if (_timeleft <= 0f) {
            var fpsValue = _accum / _frames;
            _uiTextFps.text = string.Format("{0:F2}", fpsValue);
            if (fpsValue > 40f) {
                _uiTextFps.color = Color.green;
            } else if (fpsValue > 25f) {
                _uiTextFps.color = Color.yellow;
            } else {
                _uiTextFps.color = Color.red;
            }

            _timeleft += 1f;
            _accum = 0f;
            _frames = 0;
        }
    }
}