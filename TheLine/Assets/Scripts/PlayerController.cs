﻿using UnityEngine;
using System;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	private int _lastPositionY;
	private int _scoreMult = 1;

	private Coroutine _boosterImmortalCorutine;
	private Coroutine _boosterX2Courutine;

	[SerializeField]
	private float _playerSpeed = 0.5f;

	[SerializeField]
	private float _blinkingSpeed = 0.02f;

	private Transform _cachedTransform;
	private StateController _stateController;
	private UIController _uiController;
	private Rigidbody2D _playerRigidbody;
	private Score _score;

	private bool _isImmortal = false;

	private bool _isMoving;

	public void OnStart()
	{
		_lastPositionY = 0;
	}

	public void Move()
	{
		_isMoving = true;
	}

	public void Stop()
	{
		_isMoving = false;
	}

	void Awake()
	{
		_cachedTransform = transform;
		_uiController = FindObjectOfType<UIController>();
		_stateController = FindObjectOfType<StateController>();
		_score = GameObject.FindObjectOfType<Score>();
		_playerRigidbody = _cachedTransform.gameObject.GetComponent<Rigidbody2D>();


		if (_stateController == null)
		{
			Debug.Log("StateController is null");
		}

		if (_uiController == null)
		{
			Debug.Log("UIController is null");
		}

		if (_score == null)
		{
			Debug.LogError("Score is null");
		}

		if (_playerRigidbody == null)
		{
			Debug.LogError("Rigidbody is null");
		}
	}

	void Update()
	{
		if (_isMoving)
		{
			UpdatePosition();

			UpdateScore();
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (!_isImmortal)
		{
			if (col.gameObject.tag == "Block")
			{
				_stateController.ChangeState(StateType.Dead);
				DeactivateBoostersEffect();
			}
		}
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "BoosterImmortal")
		{
			if (_boosterImmortalCorutine != null)
				StopCoroutine(_boosterImmortalCorutine);

			ActivateImmortalBooster();
			col.gameObject.SetActive(false);
		}
		else if (col.gameObject.tag == "BoosterX2")
		{
			if (_boosterX2Courutine != null)
				StopCoroutine(_boosterX2Courutine);

			ActivateScoreX2Booster();
			col.gameObject.SetActive(false);
		}


	}

	public void SetPlayerPosition(float x)
	{
		Vector2 position = _cachedTransform.position;

		position.x = x;
		_cachedTransform.position = position;
	}

	public void DeactivateBoostersEffect()
	{
		_scoreMult = 1;
		_playerRigidbody.isKinematic = false;

		if (_boosterImmortalCorutine != null)
			StopCoroutine(_boosterImmortalCorutine);

		if (_boosterX2Courutine != null)
			StopCoroutine(_boosterX2Courutine);
	}

	private void UpdatePosition()
	{
		_cachedTransform.position += Vector3.up * _playerSpeed * Time.deltaTime;
	}

	private void UpdateScore()
	{
		int position = (int)_cachedTransform.position.y;

		_score.CurrentScore += (position - _lastPositionY) * _scoreMult;
		_uiController.UpdateScore(_score.CurrentScore);

		_lastPositionY = position;
	}

	private void ActivateImmortalBooster()
	{
		_uiController.ActivateImmortalUI();
		_uiController.FlyEffectOnTheBlock();
		_playerRigidbody.isKinematic = true;

		_boosterImmortalCorutine = StartCoroutine(TimerForAction(10, _uiController.UpdateImmortalTimer, _uiController.SetActiveImmortalImmage, () =>
		 {
			 _playerRigidbody.isKinematic = false;
			 _uiController.DeactivateImmortalUI();
			 _uiController.FlyEffectUnderTheBlock();
		 }
		));
	}

	private void ActivateScoreX2Booster()
	{
		_uiController.ActivateX2UI();
		_scoreMult = 2;

		_boosterX2Courutine = StartCoroutine(TimerForAction(10, _uiController.UpdateX2Timer, _uiController.SetActiveX2Immage, () =>
		 {
			 _scoreMult = 1;
			 _uiController.DeactivateX2UI();
		 }
		));
	}

	private IEnumerator TimerForAction(float time, Action<int> updateTimer, Action<bool> setActive, Action function)
	{
		float currentTime = time;
		bool isActive = true;
		float blinkCurrent = 0;

		while (currentTime > 0)
		{
			if (currentTime < 3)
			{
				blinkCurrent += Time.deltaTime;

				if (blinkCurrent >= _blinkingSpeed)
				{
					blinkCurrent = 0;

					isActive = !isActive;
					setActive(isActive);
				}
			}

			currentTime -= Time.deltaTime;

			updateTimer((int)currentTime);

			yield return null;
		}

		function();
	}
}
