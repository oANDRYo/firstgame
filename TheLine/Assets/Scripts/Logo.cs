﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Logo : MonoBehaviour
{
	[SerializeField]
	private float _logoTime = 2f;

	void Start()
	{
		StartCoroutine(WaitTimeAndPlay());
	}

	private IEnumerator WaitTimeAndPlay()
	{
		float _currentTime = 0;

		while(_currentTime<_logoTime)
		{
			_currentTime += Time.deltaTime;

			yield return null;
		}

		SceneManager.LoadScene(1);
	}
}
