﻿using UnityEngine;

public class PlayMode : IGameState
{
	private readonly PlayerController _playerController;
	private readonly Score _score;
	private readonly UIController _uiController;
	private readonly PathController _pathController;

	public PlayMode()
	{
		_pathController = Object.FindObjectOfType<PathController>();
		_playerController = Object.FindObjectOfType<PlayerController>();
		_uiController = Object.FindObjectOfType<UIController>();
		_score = Object.FindObjectOfType<Score>();

		if (_playerController == null)
		{
			Debug.LogError("PlayerController is null");
		}

		if(_score==null)
		{
			Debug.LogError("Score is null");
		}

		if (_uiController == null)
		{
			Debug.LogError("UIController is null");
		}

		if (_pathController == null)
		{
			Debug.LogError("PathController is null");
		}
	}

	public void OnEnter()
	{
		_playerController.Move();
		_uiController.EnablePlayUI();
	}

	public void OnExit()
	{
		_playerController.Stop();
	}
}
