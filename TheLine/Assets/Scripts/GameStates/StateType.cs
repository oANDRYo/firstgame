﻿using UnityEngine;
using System.Collections;

public enum StateType
{
	Start,
	Play,
	Pause,
	Dead
}
