﻿using UnityEngine;

public class DeadMode : IGameState
{
	private readonly UIController _uiController; 
	private readonly Transform _playerTransform;
	private readonly ParticleSystem _deadEffect; 
	private readonly Transform _deadEffectTransform; 
	private readonly Score _score;
	private readonly InputController _inputController;


	public DeadMode()
	{
		_uiController = Object.FindObjectOfType<UIController>();
		_score = Object.FindObjectOfType<Score>();
		_inputController = Object.FindObjectOfType<InputController>();
		var player = GameObject.FindGameObjectWithTag("Player");
		var _deadEffectObject = GameObject.FindGameObjectWithTag("DeadEffect");


		if (_uiController == null)
		{
			Debug.LogError("UIController is null");
		}

		if (player == null)
		{
			Debug.LogError("Player is null");
		}
		else
		{
			_playerTransform = player.transform;
		}

		

		if (_score == null)
		{
			Debug.LogError("Score is null");
		}

		if (_deadEffectObject == null)
		{
			Debug.Log("DeadEffect is null");

		}
		else
		{
			_deadEffectTransform = _deadEffectObject.transform;
			_deadEffect = _deadEffectObject.GetComponent<ParticleSystem>();
		}
	}

	public void OnEnter()
	{
		_uiController.DisablePlayUI();
		_uiController.DeactivateX2UI();
		_uiController.DeactivateImmortalUI();
		_score.SaveIfBestScore();
		 
		_deadEffectTransform.position = _playerTransform.position;
		_deadEffect.Play();

		_uiController.ShowPopUpWindow(_score.CurrentScore, _score.GetBestScore(), _score.GetGameCount());
		_uiController.DisableSwipeZone();
		
		_playerTransform.gameObject.SetActive(false);
	}

	public void OnExit()
	{
		_deadEffect.Clear();
		
		_playerTransform.position = Vector3.zero;
		_playerTransform.gameObject.SetActive(true);

		_uiController.HidePopUpWindow();
		_uiController.EnableSwipeZone();
		_inputController.InDefaultOptions();
	}
}
