﻿using UnityEngine;
using System.Collections;
using System;

public class PauseMode : IGameState
{
	private readonly UIController _uiController;
	private readonly InputController _inputController;

	public PauseMode()
	{
		_inputController = GameObject.FindObjectOfType<InputController>();
		_uiController = GameObject.FindObjectOfType<UIController>();

		if (_uiController == null)
		{
			Debug.LogError("UIController is null");
		}
	}

	public void OnEnter()
	{
		_inputController.InDefaultOptions();
		_uiController.PauseTextSetActive(true);
		_uiController.PauseButtonSetActive(false);
		Time.timeScale = 0;
	}

	public void OnExit()
	{
		_uiController.PauseTextSetActive(false);
		_uiController.PauseButtonSetActive(true);
		Time.timeScale = 1;
	}
}
