﻿using UnityEngine;

public class StartMode : IGameState
{
	private readonly PlayerController _playerController;
	private readonly PathController _pathController;
	private readonly Transform _playerTransform;
	private readonly UIController _uiController;
	private readonly Score _score;

    public StartMode()
	{
		_playerController = Object.FindObjectOfType<PlayerController>();
		_pathController = GameObject.FindObjectOfType<PathController>();
		_score = Object.FindObjectOfType<Score>();
		_uiController = Object.FindObjectOfType<UIController>();
		var player = GameObject.FindGameObjectWithTag("Player");

		if (_pathController == null)
		{
			Debug.LogError("PathController is null");
		}

		if(player!=null)
		{
			_playerTransform = player.transform;
		}

		if (_uiController == null)
		{
			Debug.LogError("UIController is null");
		}

		if (_score == null)
		{
			Debug.LogError("Score is null");
		}

		if (_playerController == null)
		{
			Debug.LogError("PlayerController is null");
		}
	}

	public void OnEnter()
	{
		_score.CurrentScore = 0;
		_pathController.DeactivateAllBoosters();
		_playerController.DeactivateBoostersEffect();
		_playerTransform.position = Vector3.zero;
		_pathController.PreapareToStart();
		_uiController.EnableMenuUI();
		_uiController.FlyEffectOff();
		_uiController.MenuPlayerResult(_score.GetBestScore(), _score.GetGameCount());
	}

	public void OnExit()
	{
		_score.IncrementGameCount();
		_uiController.DisableMenuUI();
		_playerController.OnStart();
		_uiController.FlyEffectOn();
		
	}
}
