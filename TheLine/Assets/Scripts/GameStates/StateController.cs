﻿using UnityEngine;
using System.Collections.Generic;

public class StateController : MonoBehaviour
{
	private Dictionary<StateType, IGameState> _states;
	private StateType _currentType;

	public StateType CurrentState
	{
		get
		{
			return _currentType;
		}
	}

	void Start()
	{
		_states = new Dictionary<StateType, IGameState>()
		{
			{StateType.Start, new StartMode() },
			{StateType.Play, new PlayMode() },
			{StateType.Dead, new DeadMode() },
			{StateType.Pause, new PauseMode() }
		};

		_currentType = StateType.Start;

		_states[_currentType].OnEnter();
	}

	public void ChangeState(StateType type)
	{
		_states[_currentType].OnExit();
		_states[type].OnEnter();

		_currentType = type;
	}

}
