﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour
{
	private readonly string _bestScoreString = "BestScore";
	private readonly string _gameCountString = "GameCount";

	public int CurrentScore { get; set; }
	
	public void IncrementGameCount()
	{
		int score = GetGameCount();
		score++;

		PlayerPrefs.SetInt(_gameCountString, score);
	}

	public bool SaveIfBestScore()
	{
		int bestScore = GetBestScore();

		if(CurrentScore>bestScore)
		{
			PlayerPrefs.SetInt(_bestScoreString, CurrentScore);

			return true;
		}
		return false;
	}

	public int GetBestScore()
	{
		return PlayerPrefs.GetInt(_bestScoreString);
	}

	public int GetGameCount()
	{
		return PlayerPrefs.GetInt(_gameCountString);
	}
}
