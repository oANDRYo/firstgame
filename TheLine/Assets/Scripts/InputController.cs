﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputController : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
	private PlayerController _playerController;
	private Camera _mainCamera;
	private StateController _stateController;
	private UIController _uiController;

	private int _poindId;
	private bool _isHaveFinger=false;

	void Start()
	{
		StartInitialize();
	}

	public void InDefaultOptions()
	{
		_uiController.EnableSwipeZoneVisible();
		_isHaveFinger = false;
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		if (_isHaveFinger)
		{
			 return;
		}
		else
		{
			_isHaveFinger = true;
			_poindId = eventData.pointerId;
		}

		_uiController.DisableSwipeZoneVisible();

		if (!isPlayMode())
		{
			_stateController.ChangeState(StateType.Play);
		}

		UpdatePlayerPosition(eventData);
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (isPlayMode() && eventData.pointerId == _poindId)
		{
			UpdatePlayerPosition(eventData);
		}
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (_isHaveFinger && eventData.pointerId == _poindId)
		{
			_uiController.EnableSwipeZoneVisible();
			_isHaveFinger = false;
		}
	}

	private void StartInitialize()
	{
		_uiController = FindObjectOfType<UIController>();
		_playerController = FindObjectOfType<PlayerController>();
		_stateController = FindObjectOfType<StateController>();
		_mainCamera = Camera.main;

		if (_playerController == null)
		{
			Debug.LogError("PlayerController not found");
		}

		if (_stateController == null)
		{
			Debug.LogError("StateController not found");
		}

		if (_mainCamera == null)
		{
			Debug.LogError("MainCamera not found");
		}

		if (_uiController == null)
		{
			Debug.LogError("UIController not found");
		}
	}

	private bool isPlayMode()
	{
		return _stateController.CurrentState == StateType.Play;
	}

	private void UpdatePlayerPosition(PointerEventData eventData)
	{
		Vector2 temp = _mainCamera.ScreenToWorldPoint(eventData.position);

		_playerController.SetPlayerPosition(temp.x);
	}

}
