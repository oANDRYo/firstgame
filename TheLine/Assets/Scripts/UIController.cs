﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIController : MonoBehaviour
{
	[SerializeField]
	private GameObject _pauseText;

	[SerializeField]
	private GameObject _menuUI;

	[SerializeField]
	private GameObject _playUI;

	[SerializeField]
	private Text _scoreOnPlayText;

	[SerializeField]
	private Animator _popUpAnimator;

	[SerializeField]
	private Text _scoreOnDeath;

	[SerializeField]
	private Text _bestScoreOnDeath;

	[SerializeField]
	private Text _gamePlayedOnDeath;

	[SerializeField]
	private GameObject _swipeZoneText;

	[SerializeField]
	private GameObject _leftSwipeArrow;

	[SerializeField]
	private GameObject _rightSwipeArrow;

	[SerializeField]
	private Text _bestScoreInMenu;

	[SerializeField]
	private Text _gamePlayedInMenu;

	[SerializeField]
	private Text _boostX2Timer;

	[SerializeField]
	private GameObject _boostX2Image;

	[SerializeField]
	private Text _boostImmortalTimer;

	[SerializeField]
	private GameObject _boostImmortalImage;

	[SerializeField]
	private GameObject _soundOnImage;

	[SerializeField]
	private GameObject _soundOffImage;

	[SerializeField]
	private GameObject _flyEffect;

	[SerializeField]
	private GameObject _pauseButton;

	private Image _swipeImage;
	private StateController _stateController;
	private SpriteRenderer _flyEffectSpriteRenderer;

	void Start()
	{
		var clickAndSwipe = GameObject.FindGameObjectWithTag("SwipeZone");
		_stateController = FindObjectOfType<StateController>();


		if (_stateController == null)
		{
			Debug.LogError("StateController not found");
		}

		if (clickAndSwipe == null)
		{
			Debug.LogError("SwipeZone tag is not found");
		}
		else
		{
			_swipeImage = clickAndSwipe.GetComponent<Image>();

			if (_swipeImage == null)
			{
				Debug.LogError("Set Image on SwipeZone");
			}
		}

		if(_flyEffect!=null)
		{
			_flyEffectSpriteRenderer = _flyEffect.GetComponent<SpriteRenderer>();

			if(_flyEffectSpriteRenderer==null)
			{
				Debug.LogError("Set SpriteRenderer on FlyEffect");
			}
		}
	}

	void OnApplicationPause(bool isPaused)
	{
		if (isPaused && _stateController.CurrentState == StateType.Play)
		{
			_stateController.ChangeState(StateType.Pause);
		}
	}


	public void PauseTextSetActive(bool isActive)
	{
		_pauseText.SetActive(isActive);
	}

	public void PauseButtonSetActive(bool isActive)
	{
		_pauseButton.SetActive(isActive);
	}

	public void UpdateScore(int score)
	{
		_scoreOnPlayText.text = score.ToString();
	}

	public void EnableSwipeZone()
	{
		EnableSwipeZoneVisible();
		_swipeImage.gameObject.SetActive(true);
		
	}

	public void DisableSwipeZone()
	{
		SetSwipeZoneElementsActive(false);
		_swipeImage.gameObject.SetActive(false);
	}

	public void EnableSwipeZoneVisible()
	{
		SetSwipeZoneElementsActive(true);
		SetSwipeZoneAlphaChannel(1);
	}

	public void DisableSwipeZoneVisible()
	{
		SetSwipeZoneElementsActive(false);
		SetSwipeZoneAlphaChannel(0);
	}

	public void OnRestart()
	{
		_stateController.ChangeState(StateType.Start);
	}

	public void ShowPopUpWindow(int score, int bestScore, int gameCount)
	{
		_scoreOnDeath.text = score.ToString();
		_bestScoreOnDeath.text = bestScore.ToString();
		_gamePlayedOnDeath.text = gameCount.ToString();

		SetPopUpVisibleValue(true);
	}

	public void HidePopUpWindow()
	{
		SetPopUpVisibleValue(false);
	}

	public void MenuPlayerResult(int bestScore,int gamePlayed)
	{
		_bestScoreInMenu.text = bestScore.ToString();
		_gamePlayedInMenu.text = gamePlayed.ToString();
	}

	public void InPauseState()
	{
		_stateController.ChangeState(StateType.Pause);
	}

	public void DisablePlayUI()
	{
		_playUI.SetActive(false);
	}

	public void EnablePlayUI()
	{
		_playUI.SetActive(true);
	}

	public void EnableMenuUI()
	{
		_menuUI.SetActive(true);
	}

	public void DisableMenuUI()
	{
		_menuUI.SetActive(false);
	}

	public void SetActiveImmortalImmage(bool isActive)
	{
		_boostImmortalImage.SetActive(isActive);
	}

	public void SetActiveX2Immage(bool isActive)
	{
		_boostX2Image.SetActive(isActive);
	}

	public void ActivateImmortalUI()
	{
		SetActiveImmortalImmage(true);
		_boostImmortalTimer.gameObject.SetActive(true);
	}

	public void DeactivateImmortalUI()
	{
		SetActiveImmortalImmage(false);
		_boostImmortalTimer.gameObject.SetActive(false);
	}

	public void ActivateX2UI()
	{
		SetActiveX2Immage(true);
		_boostX2Timer.gameObject.SetActive(true);
	}

	public void DeactivateX2UI()
	{
		SetActiveX2Immage(false);
		_boostX2Timer.gameObject.SetActive(false);
	}

	public void UpdateImmortalTimer(int time)
	{
		_boostImmortalTimer.text = time.ToString();
	}

	public void UpdateX2Timer(int time)
	{
		_boostX2Timer.text = time.ToString();
	}

	public void SoundOn()
	{
		_soundOnImage.SetActive(true);
		_soundOffImage.SetActive(false);
	}

	public void SoundOff()
	{
		_soundOnImage.SetActive(false);
		_soundOffImage.SetActive(true);
	}

	public void FlyEffectOn()
	{
		_flyEffect.SetActive(true);
		FlyEffectUnderTheBlock();
	}

	public void FlyEffectOff()
	{
		_flyEffect.SetActive(false);
	}

	public void FlyEffectOnTheBlock()
	{
		_flyEffectSpriteRenderer.sortingLayerName = "FlyEffectOn";
	}

	public void FlyEffectUnderTheBlock()
	{
		_flyEffectSpriteRenderer.sortingLayerName = "FlyEffectUnder";
	}

	private void SetPopUpVisibleValue(bool value)
	{
		if (value)
		{
			_popUpAnimator.gameObject.SetActive(true);
			_popUpAnimator.SetBool("isPopUpVisible", value);
		}
		else
		{
			_popUpAnimator.gameObject.SetActive(false);
		}
	}

	public void OnExitClick()
	{
		Application.Quit();
	}

	private void SetSwipeZoneAlphaChannel(float alphaValue)
	{
		var color = _swipeImage.color;

		color.a = alphaValue;
		_swipeImage.color = color;
	}

	private void SetSwipeZoneElementsActive(bool isActive)
	{
		_swipeZoneText.SetActive(isActive);
		_leftSwipeArrow.SetActive(isActive);
		_rightSwipeArrow.SetActive(isActive);
	}
}
