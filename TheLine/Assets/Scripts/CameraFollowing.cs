﻿using UnityEngine;
using System.Collections;

public class CameraFollowing : MonoBehaviour {

	private Transform _cachedTransform;
	[SerializeField]
	private Transform _playerTransform;

	float _cameraMargin = 0;

	void Awake()
	{
		_cachedTransform = transform;
		_cameraMargin = _cachedTransform.position.y- _playerTransform.position.y;
	}

	void LateUpdate()
	{
		var temp = _cachedTransform.position;
		temp.y = _playerTransform.position.y + _cameraMargin;

		_cachedTransform.position = temp;
	}
}
