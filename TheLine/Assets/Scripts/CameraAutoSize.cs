﻿using UnityEngine;

public class CameraAutoSize
{
	private static Camera _mainCamera;

	public static void SetCameraCameraAutoSize(float width)
	{
		_mainCamera = Camera.main;

		if(_mainCamera==null)
		{
			Debug.LogError("MainCamera is null");
		}

		_mainCamera.orthographicSize = width / (_mainCamera.aspect * 2);
	}
}
