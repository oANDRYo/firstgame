﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathController : MonoBehaviour
{
	[SerializeField]
	private int _blockSize = 2;
	[SerializeField]
	private int _width = 7;
	[SerializeField]
	private int _height = 25;

	[SerializeField]
	private uint _minScoreForBoosters = 100;

	private Score _score;

	private BlockPool _blockPool;
	private BoxCollider2D _trigger;
	private Section[] _sections;

	private int _lastEmptyArrayPointX;
	private Vector2 _leftUpperBlockPosition;

	void Awake()
	{
		_score = Object.FindObjectOfType<Score>();
		_blockPool = new BlockPool();
		_trigger = GetComponent<BoxCollider2D>();

		if (_trigger == null)
		{
			Debug.LogError("PathController not have Colider2d");
		}

		if (_score == null)
		{
			Debug.LogError("Score is null");
		}

		CameraAutoSize.SetCameraCameraAutoSize(_blockSize * _width);
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		RebuilSectionArray();
	}

	public void PreapareToStart()
	{
		if (_sections != null)
		{
			for (int i = 0; i < _sections.Length; i++)
			{
				_blockPool.DeactivateBlockSection(_sections[i]);
			}
		}

		_sections = new Section[3];

		_leftUpperBlockPosition = new Vector2(-_width / 2f * _blockSize + _blockSize / 2f, _width / 2f * _blockSize);

		_sections[0] = GetDefaultSection();
		SpawnBlockSection(_sections[0]);

		GenerateSection(out _sections[1]);
		SpawnBlockSection(_sections[1]);

		GenerateSection(out _sections[2]);
		SpawnBlockSection(_sections[2]);

		ConfigureTriggerOptions(_height * _blockSize);
	}

	public void UpdatePathCollor(Color color)
	{
		_blockPool.UpdateBlocksColor(color);
	}

	public void DeactivateAllBoosters()
	{
		_blockPool.DeactivateAllBoosters();
	}

	private void ConfigureTriggerOptions(float y)
	{
		var vector = _trigger.size;
		vector.x = _width * _blockSize;
		_trigger.size = vector;

		_trigger.enabled = true;
	}

	private void SetTriggerPositionY(float y)
	{
		Vector2 temp = _trigger.offset;
		temp.y = y;
		_trigger.offset = temp;
	}

	private Section GetDefaultSection()
	{
		if (_width % 2 == 0)
		{
			Debug.LogError("Width must be unpaired value");
		}

		Section section = new Section(_height, _width);

		int center = _width / 2;
		_lastEmptyArrayPointX = center;

		for (int i = 0; i < _height; i++)
		{
			int left = center - i;
			int right = center + i;

			if (left < 0)
			{
				left = 0;
				right = _width - 1;
			}

			for (int j = left; j <= right; j++)
			{
				section[i, j].Type = BlockType.Empty;
			}
		}

		return section;
	}

	private void RebuilSectionArray()
	{
		_blockPool.DeactivateBlockSection(_sections[0]);

		var temp = _sections[2];
		_sections[0] = _sections[1];
		_sections[1] = _sections[2];

		GenerateSection(out _sections[2]);
		SpawnBlockSection(_sections[2]);
	}

	private void GenerateSection(out Section section)
	{
		var current = new ArrayIndex2DVector();
		section = new Section(_height, _width);

		current.I = section.Height - 1;
		current.J = _lastEmptyArrayPointX;

		section[current.I, current.J].Type = BlockType.Empty;

		while (current.I != 0)
		{
			var vectors = new List<ArrayIndex2DVector>()
			{
				ArrayIndex2DVector.Up
			};

			if (current.J > 1 && section[current.I, current.J - 1].Type == BlockType.Block)
			{
				vectors.Add(ArrayIndex2DVector.Left);
			}

			if (current.J < _width - 2 && section[current.I, current.J + 1].Type == BlockType.Block)
			{
				vectors.Add(ArrayIndex2DVector.Right);
			}

			current += SelectRandomVector(vectors);

			if (_score.CurrentScore >= _minScoreForBoosters)
			{
				section[current].Type = GetRandomBlockType();
			}
			else
				section[current].Type = BlockType.Empty;
		}

		_lastEmptyArrayPointX = current.J;

	}

	private ArrayIndex2DVector SelectRandomVector(List<ArrayIndex2DVector> vectors)
	{
		return vectors[Random.Range(0, vectors.Count)];
	}

	private BlockType GetRandomBlockType()
	{
		int value = Random.Range(0, 100);

		if (value > 1)
		{
			return BlockType.Empty;
		}
		else if (value == 0)
		{
			return BlockType.BoosterX2;
		}

		return BlockType.BoosterImmortal;
	}

	private void SpawnBlockSection(Section section)
	{
		for (int i = 0; i < section.Height; i++)
		{
			for (int j = 0; j < section.Width; j++)
			{
				if (section[i, j].Type != BlockType.Empty)
				{
					Vector2 vector = _leftUpperBlockPosition + (Vector2.down * i + Vector2.right * j) * _blockSize;

					switch (section[i, j].Type)
					{
						case (BlockType.Block):
						{
							section[i, j] = _blockPool.CreateBlock(vector);
							break;
						}
						case (BlockType.BoosterX2):
						{
							section[i, j] = _blockPool.CreateBoosterX2(vector);
							break;
						}
						default:
						{
							section[i, j] = _blockPool.CreateBoosterImmortal(vector);
							break;
						}
					}
				}
			}
		}
		SetTriggerPositionY(_leftUpperBlockPosition.y - _height * _blockSize);

		_leftUpperBlockPosition += Vector2.up * _blockSize * section.Height;
	}
}
