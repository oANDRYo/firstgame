﻿using UnityEngine;
using System.Collections.Generic;

public class Section  {

	private Block[,] _blocks;

	public int Width
	{
		get
		{
			return _blocks.GetLength(1);
		}
	}

	public int Height
	{
		get
		{
			return _blocks.GetLength(0);
		}
	}

	public Block this[ArrayIndex2DVector point]
	{
		get
		{
			return this[point.I, point.J];
		}
		set
		{
			this[point.I, point.J] = value;
		}
	}

	public Block this[int x, int y]
	{
		get
		{
			return _blocks[x, y];
		}
		set
		{
			_blocks[x, y] = value;
		}
	}
	
	public Section(int height,int width)
	{
		_blocks = new Block[height, width];

		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				_blocks[i, j] = new Block(BlockType.Block);
			}
		}
	}

	

	


}
