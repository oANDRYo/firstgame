﻿using UnityEngine;
using System.Collections;

public enum BlockType
{
	Block,
	Empty,
	BoosterX2,
	BoosterImmortal
}

public class Block
{
	public Transform Transform{get;set;}

	public BlockType Type{get;set;}

	public SpriteRenderer SpriteRenderer { get; set; }

	public Block(BlockType type)
	{
		Type = type;
	}

	public Block(BlockType type, Transform transform, SpriteRenderer spriteRenderer):this(type)
	{
		Transform = transform;
		SpriteRenderer = spriteRenderer;
	}

	public Block(BlockType type, Transform transform) : this(type)
	{
		Transform = transform;
	}
}
