﻿using UnityEngine;
using System.Collections;

public struct ArrayIndex2DVector
{
	public static ArrayIndex2DVector Up
	{
		get
		{
			return new ArrayIndex2DVector(-1, 0);
		}
	}

	public static ArrayIndex2DVector Left
	{
		get
		{
			return new ArrayIndex2DVector(0, -1);
		}
	}

	public static ArrayIndex2DVector Right
	{
		get
		{
			return new ArrayIndex2DVector(0, 1);
		}
	}

	public int I { get; set; }

	public int J { get; set; }

	/*
		------>	j
		| 00 01 02
		| 10 11 12
		| 20 21 22
		v

		i

	*/

	public ArrayIndex2DVector(int i, int j)
	{
		I = i;
		J = j;
	}

	public static ArrayIndex2DVector operator +(ArrayIndex2DVector a, ArrayIndex2DVector b)
	{
		return new ArrayIndex2DVector(a.I + b.I, a.J + b.J);
	}
}
