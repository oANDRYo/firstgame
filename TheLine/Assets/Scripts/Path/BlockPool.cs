﻿using UnityEngine;
using System.Collections.Generic;

delegate Block Function(Vector2 position);

public class BlockPool
{
	private readonly GameObject _blockPrefab;
	private readonly GameObject _boosterX2Prefab;
	private readonly GameObject _boosterImmortalPrefab;

	private List<Block> _blocksPool;
	private List<Block> _boostersX2pool;
	private List<Block> _boostersImmortalPool;

	public BlockPool()
	{
		_blocksPool = new List<Block>();
		_boostersImmortalPool = new List<Block>();
		_boostersX2pool = new List<Block>();

		_blockPrefab = Resources.Load<GameObject>("Prefabs/Block");
		_boosterImmortalPrefab = Resources.Load<GameObject>("Prefabs/Boosters/Immortal");
		_boosterX2Prefab = Resources.Load<GameObject>("Prefabs/Boosters/x2");

		if (_blockPrefab == null || _boosterX2Prefab == null || _boosterImmortalPrefab == null)
			Debug.LogError("Can not load Prefabs from Resurces");
	}

	public Block CreateBlock(Vector2 position)
	{
		return CreateElement(position, _blocksPool, InstantiateBlock);
	}

	public Block CreateBoosterX2(Vector2 position)
	{
		return CreateElement(position, _boostersX2pool, InstantiateBoosterX2);
	}

	public Block CreateBoosterImmortal(Vector2 position)
	{
		return CreateElement(position, _boostersImmortalPool, InstantiateBoosterImmortal);
	}

	private Block CreateElement(Vector2 position, List<Block> pool, Function createElement)
	{
		bool isCreated = false;
		Block createdBlock = null;

		foreach (var blockInPool in pool)
		{
			if (!blockInPool.Transform.gameObject.activeInHierarchy)
			{
				blockInPool.Transform.position = position;
				blockInPool.Transform.gameObject.SetActive(true);

				isCreated = true;
				createdBlock = blockInPool;

				break;
			}
		}

		if (!isCreated)
		{
			createdBlock = createElement(position);
		}

		return createdBlock;
	}

	private Block InstantiateBlock(Vector2 position)
	{
		var createdObject = (GameObject)Object.Instantiate(_blockPrefab, position, Quaternion.identity);
		Block block = new Block(BlockType.Block, createdObject.transform, createdObject.GetComponent<SpriteRenderer>());

		_blocksPool.Add(block);

		return block;
	}

	private Block InstantiateBoosterX2(Vector2 position)
	{
		var createdObject = (GameObject)Object.Instantiate(_boosterX2Prefab, position, Quaternion.identity);
		Block block = new Block(BlockType.BoosterX2, createdObject.transform);

		_boostersX2pool.Add(block);

		return block;
	}

	private Block InstantiateBoosterImmortal(Vector2 position)
	{
		var createdObject = (GameObject)Object.Instantiate(_boosterImmortalPrefab, position, Quaternion.identity);
		Block block = new Block(BlockType.BoosterImmortal, createdObject.transform);

		_boostersImmortalPool.Add(block);

		return block;
	}

	public void UpdateBlocksColor(Color color)
	{
		foreach (var block in _blocksPool)
		{
			block.SpriteRenderer.color = color;
		}
	}

	public void DeactivateAllBoosters()
	{
		foreach (var boosterImmortal in _boostersImmortalPool)
		{
			boosterImmortal.Transform.gameObject.SetActive(false);
		}

		foreach (var boosterX2 in _boostersX2pool)
		{
			boosterX2.Transform.gameObject.SetActive(false);
		}
	}

	public void DeactivateBlockSection(Section section)
	{
		for (int i = 0; i < section.Height; i++)
		{
			for (int j = 0; j < section.Width; j++)
			{
				if (section[i, j].Type == BlockType.Block)
				{
					section[i, j].Transform.gameObject.SetActive(false);
				}
			}
		}
	}
}
