﻿using UnityEngine;

class Audio : MonoBehaviour
{
	private string _audio = "isAudioMuted";
	private AudioSource _audioSource;
	private UIController _uiController;

	void Awake()
	{
		_uiController = FindObjectOfType<UIController>();
		_audioSource = FindObjectOfType<AudioSource>();

		if (_uiController == null)
		{
			Debug.Log("UIController is null");
		}

		if (_audioSource == null)
		{
			Debug.Log("AudioSource is null");
		}

		_audioSource.mute = GetBoolFromInt(PlayerPrefs.GetInt(_audio, 0));
		UpdateIcon(_audioSource.mute);
	}

	public void ChangeStatus()
	{
		_audioSource.mute = !_audioSource.mute;

		UpdateIcon(_audioSource.mute);
		PlayerPrefs.SetInt(_audio, GetIntFromBool(_audioSource.mute));
	}

	bool GetBoolFromInt(int i)
	{
		if (i == 0)
			return false;

		return true;
	}

	int GetIntFromBool(bool value)
	{
		if (value)
			return 1;

		return 0;
	}

	void UpdateIcon(bool isMute)
	{
		if (isMute)
		{
			_uiController.SoundOff();
		}
		else
		{
			_uiController.SoundOn();
		}
	}
}
